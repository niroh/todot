<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student/{id?}', function ($id=null) {
    if($id!=null)
    return "Hello student number ".$id;
    else
    return "No student number provided";
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id'=>$id]);
})->name('comment');

Route::get('/customers/{id?}', function ($id=null) {
    if($id!=null)
        return view('comment',['id'=>$id]);
    else
        return view('nocomment');
})->name('customers');

Route::resource('todos', 'TodotController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
